function fillData(data){
	//parameters to include here
	//sessionId  - 1
	//versionNumber -  2
	//currentdate -  3
	//cookieId -  4 
	//expires -  5
	var receivedData = data.split("||");

	$("#message").text(receivedData[0].trim());
	$("#sessionId").text(receivedData[1].trim());
	$("#versionNumber").text(receivedData[2].trim());
	$("#currentdate").text(receivedData[3].trim());
	$("#cookieId").text(receivedData[6].trim());
	$("#expiresDate").text(receivedData[5].trim());
	$("#serverReadFrom").text(receivedData[7].trim());
	$("#AMI_Index").text(receivedData[8].trim());
	$("#rebootNumber").text(receivedData[9].trim());
	$("#domain").text(receivedData[10].trim());
}

$(function(){
	$.ajax({
		url : "/LSIProject/ManageSession",
		type : "GET",
		datatype : "text",
		data : {
			param : "initialPageLoad"
		},
		success : function( data, textstatus, jqXHR){

			fillData(data);

		},
		error : function( jqXHR, textstatus, errorThrown){
			
		}
	});
});

$("#refresh").click(function(){
	$.ajax({
		url : "/LSIProject/ManageSession",
		type : "GET",
		datatype : "text",
		data : {
			param : "refresh"
		},
		success : function( data, textstatus, jqXHR){
			fillData(data);
		},
		error : function( jqXHR, textstatus, errorThrown){
			
		}
	});
});



$("#replace").click(function(){
	var newmessage = $('#replaceMessage').val().trim();
	if( newmessage.length == 0 ){
		alert("Message text is empty");
	}else{
		if( newmessage.length > 512 ){
			newmessage = newmessage.substring(0,511);
		}
		$.ajax({
			url : "/LSIProject/ManageSession",
			type : "GET",
			datatype : "text",
			data : {
				param : "replace",
				message : newmessage
			},
			success : function( data, textstatus, jqXHR){
				fillData(data);
			},
			error : function( jqXHR, textstatus, errorThrown){
				
			}
		});
	}
	document.getElementById("replaceMessage").value= ""
});



$("#logout").click(function(){
	$.ajax({
		url : "/LSIProject/ManageSession",
		type : "GET",
		datatype : "text",
		data : {
			param : "logout"
		},
		success : function( data, textstatus, jqXHR){
			fillData(data);
		},
		error : function( jqXHR, textstatus, errorThrown){
			
		}
	});
});
