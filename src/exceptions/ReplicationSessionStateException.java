package exceptions;

public class ReplicationSessionStateException extends Exception {
	
	/**
	 *  User defined exception class to handle error state
	 *  during servlet request processing
	 */
	
	
	private static final long serialVersionUID = 1L;
	private String message;

	    public ReplicationSessionStateException (String message){
	        this.message = message;
	    }

	    // Overrides Exception's getMessage()
	    @Override
	    public String getMessage(){
	        return message;
	    }


}