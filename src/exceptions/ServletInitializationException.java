package exceptions;

public class ServletInitializationException extends Exception {
	
	 /**
	 *  User defined exception class to handle error state
	 *  during application initialization
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	    public ServletInitializationException (String message){
	        this.message = message;
	    }

	    // Overrides Exception's getMessage()
	    @Override
	    public String getMessage(){
	        return message;
	    }

}
