package rpc;

import java.util.ArrayList;
import java.util.List;

import SessionHandlers.SessionObj;
/**
 * POJO used for Storing the responses from RPCManager.
 * 
 * @author VISHAL,SAHANA,KARTHIK
 *
 */
public class RPCReplyData {

	SessionObj sessionObj;
	long serverNumberReadFrom;
	List<Long> replicatedServers = new ArrayList<Long>();
	
	
	public SessionObj getSessionObj() {
		return sessionObj;
	}


	public void setSessionObj(SessionObj sessionObj) {
		this.sessionObj = sessionObj;
	}


	public long getServerNumberReadFrom() {
		return serverNumberReadFrom;
	}


	public void setServerNumberReadFrom(long serverNumberReadFrom) {
		this.serverNumberReadFrom = serverNumberReadFrom;
	}


	public List<Long> getReplicatedServers() {
		return replicatedServers;
	}


	public void setReplicatedServers(List<Long> replicatedServers) {
		this.replicatedServers = replicatedServers;
	}


	public RPCReplyData(SessionObj sessionObj, long serverNumberReadFrom, List<Long> replicatedServers) {
		// TODO Auto-generated constructor stub
		this.sessionObj = sessionObj;
		this.serverNumberReadFrom = serverNumberReadFrom;
		this.replicatedServers = replicatedServers;
	}
	
	
	
}
