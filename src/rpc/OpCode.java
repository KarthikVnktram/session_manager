package rpc;
/**
 * Enum for READ AND WRITE
 * 
 * @author VISHAL,KARTHIK,SAHANA
 *
 */
public enum OpCode {
	READ("READ"),
	WRITE("WRITE");

	private String opCode;

	private OpCode(String s) {
		opCode = s;
	}

	public String getOpCode() {
		return opCode;
	}

	public void setOpCode(String opCode) {
		this.opCode = opCode;
	}


}
