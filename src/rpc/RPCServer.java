package rpc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import Session.ManageSession;
import SessionHandlers.SessionObj;
import SessionHandlers.SessionUtills;
/*
 * RPC Server runs continuously which is started when Project is deployed 
 * onto Tomcat Server.
 */
public class RPCServer extends Thread {

	DatagramSocket serverSocket = null;

	public RPCServer(){
		try {
			serverSocket = new DatagramSocket(SessionUtills.UDP_COMMUNICATION_PORT);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	/**
	 * On new Request check the OpCode(READ/WRITE) and accordingly perform action
	 */
	@Override
	public void run(){
		System.out.println("Server Started");

		while(true)
		{
			try{
				byte[] receiveData = new byte[SessionUtills.UDP_DATA_PACKET_SIZE];

				DatagramPacket receivedPacket = new DatagramPacket(receiveData, receiveData.length);

				try {
					serverSocket.receive(receivedPacket);
				} catch (IOException e) {
					e.printStackTrace();
				}

				InetAddress returnAddr = receivedPacket.getAddress();
				int returnPort = receivedPacket.getPort();

				String newString=new String(receivedPacket.getData());
				OpCode operationCode=RPCUtils.getOpCode(newString);
				String CallId=RPCUtils.getCallID(newString);


				byte[] outBuf = new byte[SessionUtills.UDP_DATA_PACKET_SIZE];

				switch( operationCode ) {
				case READ:
					String sessionId_version=RPCUtils.getSession_Version(newString);
					outBuf = SessionRead(CallId,sessionId_version);
					break;

				case WRITE:
					SessionObj sessionObj=RPCUtils.getSessionObject(newString);
					outBuf = SessionWrite(CallId, sessionObj);
					break;
				}

				DatagramPacket sendPacket =
						new DatagramPacket(outBuf, outBuf.length, returnAddr, returnPort);
				try {
					serverSocket.send(sendPacket);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}catch(Exception e){

			}
		}
	}

	/**
	 * Write the session Object into current server's Session State Table.

	 * @param callId
	 * @param sessionObj
	 * @return
	 */
	private byte[] SessionWrite(String callId, SessionObj sessionObj) {
		String replyString = "";

		if(sessionObj!=null){
			ManageSession.sessionTable.putSessionToTable(SessionUtills.generateSessionKey(sessionObj), sessionObj);
			replyString=RPCClient.TRUE+SessionUtills.DELIMITER+callId;
		}else{
			replyString=RPCClient.FALSE+SessionUtills.DELIMITER+callId;
		}

		return replyString.getBytes();
	}

	/**
	 * Read Session Object from current Servers Session Table 
	 * 
	 * @param callId
	 * @param sessionId_version
	 * @return
	 */
	private byte[] SessionRead(String callId, String sessionId_version) {
		SessionObj session =ManageSession.sessionTable.getSessionFromTable(sessionId_version.trim());
		String replyString = "";
		if(session!=null){
			replyString=RPCClient.TRUE+SessionUtills.DELIMITER+callId+SessionUtills.DELIMITER+session;
		}else{
			replyString=RPCClient.FALSE+SessionUtills.DELIMITER+callId+SessionUtills.DELIMITER+session;
		}
		return replyString.getBytes();
	}

}
