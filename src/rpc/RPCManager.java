package rpc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;

import Session.ManageSession;
import SessionHandlers.CookieUtils;
import SessionHandlers.ServerPool;
import SessionHandlers.SessionObj;
import SessionHandlers.SessionUtills;
import exceptions.ReplicationSessionStateException;

public class RPCManager {
	
	Logger logger =  Logger.getLogger(this.getClass().getName());

	/**
	 * Sends the Read request to R severs and responds back on first successfull READ. 
	 * 
	 * @param cookieString
	 * @return
	 * @throws ReplicationSessionStateException
	 */
	public synchronized RPCReplyData readSessionData(String cookieString) throws ReplicationSessionStateException{

		RPCClient rpcClient = new RPCClient();
		String[] cookieContents = cookieString.split(CookieUtils.COOKIE_DELIMITER);
		String sessionID = cookieContents[0]+ CookieUtils.COOKIE_DELIMITER+cookieContents[1]+CookieUtils.COOKIE_DELIMITER+cookieContents[2];
		String versionNumber = cookieContents[3];
		long seed = System.nanoTime();

		List<Long> replicatedServers = new ArrayList<Long>();
		for(int i = 4 ; i< cookieContents.length;i++){
			replicatedServers.add(Long.parseLong(cookieContents[i]));
		}

		Long amiLaunchIndex = SessionUtills.getCurrentAmiLaunchIndex();

		Collections.shuffle(replicatedServers, new Random(seed));
		
		logger.debug("My shuffled list is "+(replicatedServers));
		//if not present in current server try in others
		for(int i =0; i <SessionUtills.R; i++){ 

			long serverId = replicatedServers.get(i);

			if(serverId == amiLaunchIndex ){
				
				logger.debug("checking in current machine");
				SessionObj sessionObj= ManageSession.sessionTable.getSessionFromTable(sessionID+CookieUtils.COOKIE_DELIMITER+versionNumber);

				if(sessionObj != null ){
					//can return at this point
					logger.debug("Found session object in current machine");

					RPCReplyData rpcReplyData = new RPCReplyData(sessionObj, serverId, replicatedServers);
					return rpcReplyData;
				}else {
					continue;
				}

			}

			logger.debug("session object Not Found in current machine");

			String ipAddress = ServerPool.getInstance().getIPAddressFromServerPool(serverId);
			String callId = getCallId();
			//data is callId, OpCode, session_version, send_Message

			String data = getReadData(callId,sessionID,versionNumber) ; 
			try {

				String replyFromClient = rpcClient.getSessionFromServer(ipAddress, data, callId);
				if(replyFromClient == null){
					logger.debug("Server did not reply back for AMI Index-->"+ ipAddress);

					continue;
				}


				SessionObj sessionObj = SessionUtills.getSessionObjectFromString(replyFromClient);						
				RPCReplyData rpcReplyData = new RPCReplyData(sessionObj, serverId, replicatedServers);
				logger.debug("Server reply back for AMI Index-->"+ ipAddress);

				return rpcReplyData;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println("Couldn't read from R distinct servers.. Need to throw exception");
		throw new ReplicationSessionStateException("Unable to read consistent session state across "
				+ SessionUtills.R + " servers. Retry a new session");

	}


	/**
	 * Replicates the new Session Object across WQ instances.
	 * 
	 * @param sessionObj
	 * @return
	 * @throws ReplicationSessionStateException
	 */
	//already increment sessionObj and send it to this guy so that its replicated.
	public synchronized List<Long> replicateSessionData(SessionObj sessionObj) throws ReplicationSessionStateException{

		//  first pick WQ guys at random. try out. if not , pick more and try. if failure, return false and redirect to error page
		//	List<Long> shuffledReplicatedServers = SessionUtills.getRamdomShuffledServers(allServersInPool);
		long currentAmiIndex = SessionUtills.getCurrentAmiLaunchIndex();
		long currentReplicationcounter = 0;
		long seed = System.nanoTime();

		Set<Long> newReplicatedServers = new HashSet<Long>();

		List<Long> replicatedServers = new ArrayList<Long>();

		for(long j=0 ; j < SessionUtills.N ; j++){
			replicatedServers.add(j);
		}

		Collections.shuffle(replicatedServers,new Random(seed));
		logger.debug("Shuffled Write List of machines-->"+ replicatedServers);


		for(int i= 0; i < SessionUtills.W ; i++){//currentReplicationcounter < SessionUtills.WQ && currentRandCount < SessionUtills.W){

			if(currentReplicationcounter == SessionUtills.WQ){
				logger.debug("Replicated data across-->"+ newReplicatedServers);

				break;
			}

			long serverNumber = replicatedServers.get(i);
			logger.debug("Trying to write data to Server "+ serverNumber);

			if( serverNumber == currentAmiIndex ){
				addTheSessionObjToCurrentMachine(sessionObj);
				currentReplicationcounter++;
				newReplicatedServers.add(currentAmiIndex);
				logger.debug("Data written to Current Server "+ serverNumber);

				continue;

			}

			RPCClient rpcClient = new RPCClient();

			String ipAddress = ServerPool.getInstance().getIPAddressFromServerPool(serverNumber);
			String callId = getCallId();
			String data = getWriteData(callId,sessionObj ) ; 
			try {
				String replyFromServer = rpcClient.getSessionFromServer(ipAddress, data, callId);
				if(replyFromServer != null){
					currentReplicationcounter++;
					newReplicatedServers.add(serverNumber);
					logger.debug("Data written to Current Server "+ ipAddress);

				}
			
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if(currentReplicationcounter < SessionUtills.WQ){
			// couldn't write to the needed count of servers
			// throw exception 
			throw new ReplicationSessionStateException("Unable to replicate session state across "
					+ SessionUtills.WQ + " servers. Retry a new session.");
			//return null;
		}
		return new ArrayList<Long>(newReplicatedServers);

	}

	private void addTheSessionObjToCurrentMachine(SessionObj sessionObj) {
		// TODO Auto-generated method stub

		ManageSession.sessionTable.putSessionToTable(SessionUtills.generateSessionKey(sessionObj), sessionObj);

	}


	private String getCallId(){
		String ranDomUUID= UUID.randomUUID().toString();
		ranDomUUID = ranDomUUID.replaceAll("-", "");
		return ranDomUUID;
	}

	@SuppressWarnings("unused")
	private boolean isDataPresentInCurrentServer(List<Long> replicatedServers){
		long currentServerIndex = SessionUtills.getCurrentAmiLaunchIndex();
		if(replicatedServers.contains(currentServerIndex)){
			return true;
		}
		return false;

	}

	private String getReadData(String callId,String sessionID, String versionNumber){
		String data = callId+SessionUtills.DELIMITER+ OpCode.READ + SessionUtills.DELIMITER +sessionID+"_"+versionNumber ; 
		return data;
	}


	private String getWriteData(String callId,SessionObj sessionObj){
		String data = callId+SessionUtills.DELIMITER+ OpCode.WRITE + SessionUtills.DELIMITER +sessionObj ; 
		return data;
	}


	/**
	 * Replicate Session Object acroos already available WQ instances 
	 * with Expiration Time=Current Time on Logout
	 * 
	 * @param servers
	 * @param sessionObj
	 */
	public synchronized void replicateSessionDataForLogout(List<Long> servers,SessionObj sessionObj){

		long currentAmiIndex = SessionUtills.getCurrentAmiLaunchIndex();

		for(int serverIndex=0;serverIndex<servers.size();serverIndex++){

			if(servers.get(serverIndex)==currentAmiIndex){
				addTheSessionObjToCurrentMachine(sessionObj);
				continue;
			}

			RPCClient rpcClient = new RPCClient();

			String ipAddress = ServerPool.getInstance().getIPAddressFromServerPool(servers.get(serverIndex));
			String callId = getCallId();
			String data = getWriteData(callId,sessionObj ) ; 
			try {
				rpcClient.getSessionFromServer(ipAddress, data, callId);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


}
