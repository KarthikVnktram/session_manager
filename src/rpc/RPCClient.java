package rpc;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import org.apache.log4j.Logger;
import SessionHandlers.SessionUtills;

/**
 * RPCClient for communicating with Server.
 * 
 * @author VISHAL,KARTHIK,SAHANA
 *
 */
public class RPCClient {

	//5 seconds
	public static final int SOCKET_TIME_OUT_MS = 1 * 100;

	protected static final String TRUE  =  "T";

	protected static final String FALSE = "F";

	Logger logger =  Logger.getLogger(this.getClass().getName());

	/**
	 * Creates a new DatagramPacket with marshalled session object 
	 * and communicates the same to Server.
	 * 
	 * @param ipAddress
	 * @param data
	 * @param callId
	 * @return
	 * @throws IOException
	 */
	public String getSessionFromServer(
			String ipAddress, 
			String data, 
			String callId) throws IOException{
		boolean continueProcessDueToIOException=false;
		@SuppressWarnings("unused")
		int processingCount=0;
		do{
			continueProcessDueToIOException=false;
			DatagramSocket rpcSocket = new DatagramSocket(); 
			rpcSocket.setSoTimeout(SOCKET_TIME_OUT_MS);
			try {

				InetAddress IP = InetAddress.getByName(ipAddress);
				byte[] outputBuffer = new byte[SessionUtills.UDP_DATA_PACKET_SIZE];
				outputBuffer = data.getBytes();

				DatagramPacket packetToSend = new DatagramPacket(outputBuffer, outputBuffer.length, IP, SessionUtills.UDP_COMMUNICATION_PORT);
				rpcSocket.send(packetToSend);

				byte [] inputBuffer = new byte[SessionUtills.UDP_DATA_PACKET_SIZE];
				DatagramPacket packetToReceive = new DatagramPacket(inputBuffer, inputBuffer.length);

				do{	
					packetToReceive.setLength(inputBuffer.length);
					rpcSocket.receive(packetToReceive);
				}while(checkReceivedPacket( new String(inputBuffer) ,  callId) == false);

				return new String(inputBuffer);

			} catch (SocketTimeoutException e) {
				logger.debug("Socket Time out Exception occured "+e.getMessage());
				return null;

			} catch (IOException e) {
				processingCount++;
				continueProcessDueToIOException=true;
				logger.debug("Socket Time IO Exception occured " + e.getMessage());
				return null;

			}finally{

				rpcSocket.close();
			}
		}while(continueProcessDueToIOException == true && processingCount<3);

	}

	private boolean checkReceivedPacket(String response, String callId){

		String[] responseString = response.split(SessionUtills.SPLIT_DELIMITER);
		String dataFound = responseString[0];
		if( dataFound.equals(TRUE)){
			String callerIdReturned = responseString[1];
			if(callerIdReturned.trim().equals(callId.trim())){
				return true;
			} else{
				return false;
			}

		} else if(dataFound.equals(FALSE)){

			return false;

		} else{
			return false;
		}



	}

}
