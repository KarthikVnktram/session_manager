package rpc;

import SessionHandlers.SessionObj;
import SessionHandlers.SessionUtills;

public class RPCUtils {

	protected static String getSessionMessage(String newString) {
		try{
			return newString.split(SessionUtills.SPLIT_DELIMITER)[3];
		}catch(Exception e){
			return null;
		}
	}

	protected static SessionObj getSessionObject(String newString) {
//		String[] sessionObjs = newString.split(SessionUtills.SPLIT_DELIMITER);
//		StringBuilder stringBuilder = new StringBuilder();
//		for(int i=2; i< sessionObjs.length;i++){
//			stringBuilder.append(sessionObjs[i]);
//			if(i != sessionObjs.length-1){
//				stringBuilder.append(SessionUtills.DELIMITER);
//			}
//			
//		}
		
		return SessionUtills.getSessionObjectFromString(newString);
	}

	protected static String getCallID(String newString) {
		return newString.split(SessionUtills.SPLIT_DELIMITER)[0];
	}

	protected static OpCode getOpCode(String newString) {
		String opcode=newString.split(SessionUtills.SPLIT_DELIMITER)[1];
		if(opcode.equalsIgnoreCase(OpCode.READ.toString()))
			return OpCode.READ;
		else
			return OpCode.WRITE;
	}

	public static String getSession_Version(String newString) {
		return newString.split(SessionUtills.SPLIT_DELIMITER)[2];
	}

	
}
