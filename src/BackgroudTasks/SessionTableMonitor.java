package BackgroudTasks;

import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import Session.ManageSession;
import SessionHandlers.SessionObj;
import SessionHandlers.SessionUtills;


/**
 * This is a class to remove timed out sessions from the Session Table
 * 
 * This is a class which extends the TimerTask Thread
 * 
 * This a task which is scheduled for repeated execution by a Timer
 * 
 * @author Karthik
 *
 */
public class SessionTableMonitor extends TimerTask {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Session Table Monitor Called");
		ConcurrentHashMap<String,SessionObj > concurrentMap = ManageSession.sessionTable.getSessionMap();
		for(String key: concurrentMap.keySet()){

			SessionObj obj = concurrentMap.get(key);
			synchronized (obj) {

				if(SessionUtills.hasSessionExpired(obj.getExpirationTime())){
					System.out.println("Removing Session with session identifier "+ key +" Message"+obj.getMessage());
					concurrentMap.remove(key);
				}

			}

		}
	}



}
