package BackgroudTasks;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


/**
 * This is the class which initializes the Session Table Monitor Thread.
 * 
 * This is being started when tomcat starts initially by capturing 
 * 
 * the Servelet Context Event on Context Created
 * 
 * 
 * @author Karthik
 *
 */
@WebListener
public class SessionTableMonitorInitializer implements ServletContextListener{

	public static int TIMER_SCHEDULE_MILLISECONDS = 90000;
	Timer timer = new Timer();
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		timer.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
		SessionTableMonitor sessionTableMonitor = new SessionTableMonitor();
		timer.schedule(sessionTableMonitor,0,TIMER_SCHEDULE_MILLISECONDS);
		
	}

}
