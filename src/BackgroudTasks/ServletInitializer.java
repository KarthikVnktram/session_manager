package BackgroudTasks;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import SessionHandlers.ServerPool;
import SessionHandlers.ServerUtils;
import SessionHandlers.SessionUtills;
import exceptions.ServletInitializationException;
import rpc.RPCServer;

/**
 * Application Lifecycle Listener implementation class ServletInitializer
 *
 */
@WebListener
public class ServletInitializer implements ServletContextListener {
	
	// context attribute for applying filter	
	private static boolean initializationStatus = true;	
	public static final String initializationAttr = "isInitializationSuccessful";
	
	Logger logger =  Logger.getLogger(this.getClass().getName());

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {	
		ServerUtils.initServerPool(); 	
		try {
			
			ServerUtils.setCurrentServerAMIIndex();
			ServerUtils.setCurrentServerRebootNumber();
			ServerUtils.setResiliencyCount();
			ServerUtils.setFaultTolerantParameters();
			new Thread(new RPCServer()).start();
			logger.debug("Successfully completed all the initializations");
			logger.debug("F=>"+SessionUtills.F);
			logger.debug("N=>"+SessionUtills.N);
			logger.debug("R=>"+SessionUtills.R);	
			logger.debug("W=>"+SessionUtills.W);
			logger.debug("WQ=>"+SessionUtills.WQ);
			
			
		} catch (ServletInitializationException e) {
			// TODO Auto-generated catch block
			logger.debug("Error in the initialization phase, setting the status of initilaization to false");
			logger.error(e.getMessage());
			initializationStatus = false;			
			e.printStackTrace();
		}
		
		ServletContext context = event.getServletContext();
		context.setAttribute(initializationAttr, initializationStatus);
		
	}
	
}
