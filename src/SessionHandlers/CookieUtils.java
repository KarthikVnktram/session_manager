package SessionHandlers;

import java.util.ArrayList;
import java.util.List;

public class CookieUtils {

	public static String COOKIE_DELIMITER = "_";
	
	
	//use this to set the value of cookie everytime 
	// 0 - sessionID
	// 1 - sessionVersion
	// 2 to n list of servers
	public static String getCookieString(SessionObj sessionObj,List<Long> replicatedServers){
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(sessionObj.getUniqueIdentifier())
					 .append(COOKIE_DELIMITER)
					 .append(sessionObj.getVersion());
		if(replicatedServers != null){
			for(Long server: replicatedServers){
				stringBuilder.append(COOKIE_DELIMITER)
							  .append(server);
			}			
		}
		
		return stringBuilder.toString();
	}
	
	/*
	 *  Returns a list of server AMI index on which the session state is replicated
	 */
	public static List<Long> getListOfReplicatedServersFromCookie(String cookieString){
		
		String[] cookieContents = cookieString.split(COOKIE_DELIMITER);
		List<Long> replicatedServers = new ArrayList<Long>();
		for(int i =4 ; i< cookieContents.length;i++){
			replicatedServers.add(Long.parseLong(cookieContents[i]));
		}
		return replicatedServers;
	}
	
	

}
