package SessionHandlers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Session.SessionCounter;

/**
 * This class represents the session object.
 * This consists of 
 * 
 * 1. Unique Identifier
 * 2. Version Number
 * 3. The message is provided by the client or the default message stored per session
 * 4. The expiration time of the session
 * 
 * @author Karthik
 *
 */
public class SessionObj {

	String uniqueIdentifier;
	long version;
	String message;
	Calendar expirationTime;
	

	/**
	 * Constructor - Contains the logic to generate UID as well
	 * @param message
	 * @param expirationTime
	 */
	public SessionObj(String message, Calendar expirationTime) {
		super();
//		UID uid = new UID();
		long serverId = SessionUtills.getCurrentAmiLaunchIndex(); //This is the ami launch index of current machine.Read from cached data
		long rebootId = SessionUtills.getCurrentRebootNumber();//This is the reboot_id stored in a particular file in the instance. Read from cached Data
		this.uniqueIdentifier = serverId+CookieUtils.COOKIE_DELIMITER+rebootId+CookieUtils.COOKIE_DELIMITER+String.valueOf(SessionCounter.getInstance().incrementAndGet());
//		this.uniqueIdentifier = uid.toString();
		this.version = 1;
		this.message = message;
		this.expirationTime = expirationTime;
	}
	
	
	public SessionObj(SessionObj sessionObj){
		this.uniqueIdentifier = sessionObj.uniqueIdentifier;
		this.version = sessionObj.version;
		this.message = sessionObj.message;
		this.expirationTime = sessionObj.expirationTime;
	}
	
	public SessionObj(){
		
	}
	
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public String getMessage() {
		return message;
	}
	public synchronized void setMessage(String message) {
		this.message = message;
	}
	public Calendar getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(Calendar expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	
	public void incrementVersion(){
		this.version += 1;
	}
	
	public void updateTimeOut(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, SessionUtills.REFRESH_EXP_TIME);
		calendar.add(Calendar.MILLISECOND, SessionUtills.REFRESH_DELTA);
		this.expirationTime = calendar;
	}

	//Message + uniqueId + version+ currentTime + cookieID  + expiration time 
	@Override
	public String toString() {
		return getMessage()+SessionUtills.DELIMITER+getUniqueIdentifier()+SessionUtills.DELIMITER+getVersion()+
				SessionUtills.DELIMITER+getCalendarObjectAsString(Calendar.getInstance())+SessionUtills.DELIMITER+getUniqueIdentifier()+"_"+getVersion()
				+SessionUtills.DELIMITER+getCalendarObjectAsString(getExpirationTime());

	}
	
	private String getCalendarObjectAsString(Calendar calendar){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
		String dateAsString = formatter.format(calendar.getTime());
		return dateAsString;
	}
	
	public Calendar getCalendarObjectFromString(String string){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
		Date date = null;
		try {
			date = formatter.parse(string);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
		
	}
	
}
