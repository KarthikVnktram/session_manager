package SessionHandlers;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import Session.ManageSession;

/**
 * 
 * A Class consisting of all the static variables and utility method 
 * which are needed for session management
 * 
 * @author Karthik
 *
 */
public class SessionUtills {

	public static String PARAM = "param";
	public static String REFRESH = "refresh";
	public static String REPLACE = "replace";
	public static String LOGOUT = "logout";
	public static String INITIALLOAD = "initialPageLoad";
	public static String MESSAGESTRING = "message";


	public static String INITIAL_MESSAGE = "Hello User";
	public static String DELIMITER = "||";
	public static String SPLIT_DELIMITER = "\\|\\|";
	public static final int UDP_DATA_PACKET_SIZE=512;
	public static final int UDP_COMMUNICATION_PORT=5300;
	
	public static int REFRESH_EXP_TIME=10;
	public static int REFRESH_DELTA=100;
	
	
	public static long R = 1;
	public static long WQ = 1;
	public static long W = 1;
	public static long N = 1;
	public static long F = 0;
	
	
	
	/**
	 * Utility method to add session to table
	 * 
	 * @param uniqueId
	 * @param sessionObj
	 * @param sessionTable
	 */
	public static void addSessionToTable(String uniqueId, SessionObj sessionObj,SessionTable sessionTable){
		sessionTable.putSessionToTable(uniqueId, sessionObj);
	}

	/**
	 * Utility method to check if the session has expired
	 * 
	 * @param cal
	 * @return
	 */
	public static boolean hasSessionExpired(Calendar cal){

		boolean hasExpired = false;
		Calendar currentTime = Calendar.getInstance();
		if(currentTime.after(cal)){
			hasExpired = true;
		}
		return hasExpired;
	}

	
	public static String generateSessionKey(SessionObj sessionObj){
		return sessionObj.getUniqueIdentifier()+"_"+sessionObj.getVersion();
	}

	public static long getCurrentAmiLaunchIndex() {
		return ManageSession.currentServerAMIIndex;
	}
	
	public static long getCurrentRebootNumber() {
		return ManageSession.currentServerRebootNumber;
	}
	
	/**
	 * performs shuffle on the list of WQ servers
	 * @param replciatedServers
	 * @return
	 */
	public static List<Long> getRamdomShuffledServers(List<Long> replciatedServers){
		long seed = System.nanoTime();
		
		Collections.shuffle(replciatedServers, new Random(seed));
		return replciatedServers;
	}

	public static SessionObj getSessionObjectFromString(String replyFromServer){
		SessionObj sessObj = new SessionObj();
		String[] reply = replyFromServer.split(SessionUtills.SPLIT_DELIMITER);
		//Message + uniqueId + version+ currentTime + cookieID  + expiration time 
		String message = reply[2];
		String uniqueId = reply[3];
		String version = reply[4];
		//	String currentTime = reply[5];
		//	String cookieId = reply[6];
		String expirationTime = reply[7];
		sessObj.setMessage(message);
		sessObj.setVersion(Long.parseLong(version));
		sessObj.setUniqueIdentifier(uniqueId);
		sessObj.setExpirationTime(sessObj.getCalendarObjectFromString(expirationTime));
		return sessObj;

	}
	
}
