package SessionHandlers;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * This class represents the SessionTable Object. 
 * 
 * Internally session table is designed to contain a SessionMap
 * 
 * the map is a mapping from (session ID ->TO-> Session Object)
 * 
 * 
 * 
 * @author Karthik
 *
 */
public class SessionTable {

	ConcurrentHashMap<String, SessionObj> sessionMap = new ConcurrentHashMap<String, SessionObj>();

	/**
	 * Constructor
	 */
	public SessionTable(){
		
	}
	
	/**
	 * getter for sessionMap
	 * 
	 * @return
	 */
	public ConcurrentHashMap<String, SessionObj> getSessionMap() {
		return sessionMap;
	}

	/**
	 * Method to get the session from the table using the session ID
	 * 
	 * @param key
	 * @return
	 */
	public SessionObj getSessionFromTable(String key){
		if(key != null && sessionMap.containsKey(key)){
			return sessionMap.get(key);	
		}
		return null;
	}
	
	/**
	 * 
	 * Method to put the session object back into the session table
	 * @param key
	 * @param sessionObj
	 * @return
	 */
	public void putSessionToTable(String key, SessionObj sessionObj){
		sessionMap.put(key, sessionObj);
	}
	
	/**
	 * Method to delete the session from the session table using the unique ID of the session
	 * 
	 * @param key
	 */
	public void deleteSession(String key){
		sessionMap.remove(key);
	}
	
}
