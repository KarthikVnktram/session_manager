package SessionHandlers;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * This class represents the SessionTable Object. 
 * 
 * Internally session table is designed to contain a SessionMap
 * 
 * the map is a mapping from (session ID ->TO-> Session Object)
 * 
 * 
 * 
 * @author Karthik
 *
 */
public class ServerPool {

	ConcurrentHashMap<Long, String> serverPoolMap = new ConcurrentHashMap<Long, String>();

	private static ServerPool pool=new ServerPool();
	/**
	 * Constructor
	 */
	private ServerPool(){
	}
	
	public static ServerPool getInstance(){
		return pool;
	}
	
		
	/**
	 * getter for sessionMap
	 * 
	 * @return
	 */
	public ConcurrentHashMap<Long, String> getServerPoolMap() {
		return serverPoolMap;
	}

	/**
	 * Method to get the session from the table using the session ID
	 * 
	 * @param key
	 * @return
	 */
	public String getIPAddressFromServerPool(Long amiLaunchIndex){
		if(amiLaunchIndex != null && serverPoolMap.containsKey(amiLaunchIndex)){
			return serverPoolMap.get(amiLaunchIndex);	
		}
		return null;
	}
	
	/**
	 * 
	 * Methood to put the session object back into the session table
	 * @param key
	 * @param sessionObj
	 * @return
	 */
	public void putToServerPool(Long amiLaunchIndex, String Ipaddress){
		serverPoolMap.put(amiLaunchIndex, Ipaddress);
	}
	
	/**
	 * Method to delete the session from the session table using the unique ID of the session
	 * 
	 * @param key
	 */
	public void deleteFromServerPool(String amiLaunchIndex){
		serverPoolMap.remove(amiLaunchIndex);
	}
	
}
