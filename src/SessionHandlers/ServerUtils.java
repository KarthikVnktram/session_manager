package SessionHandlers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Session.ManageSession;
import exceptions.ServletInitializationException;

public class ServerUtils {

	private final static String REBOOT_FILE_PATH="/var/tmp/reboot.txt";
	private final static String AMI_LAUNCH_INDEX_FILEPATH ="/var/tmp/ami-launch-index.txt";
	private final static String RESILIENCY_FACTOR ="/var/tmp/resilienceCount.txt";
	private final static String SERVERS_JSON_FILE_PATH="/var/tmp/serverEntries.json";
	
	/*	public static void setCurrentServerAMIIndex() throws ServletInitializationException{	
		String url = "http://169.254.169.254/latest/meta-data/ami-launch-index";

		StringBuffer response;
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");


			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response.toString());
			ManageSession.currentServerAMIIndex = Integer.parseInt(response.toString());

		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletInitializationException("Failed to get the server's AMI-Launch Index");
		}
	}*/
	
	/**
	 * Method to read the server AMI index from server's file system.
	 * Sets the static currentServerAMIIndex variable to this value 
	 * @throws ServletInitializationException
	 */
	public static void setCurrentServerAMIIndex() throws ServletInitializationException{

		try {

			/*BufferedReader br=new BufferedReader(new FileReader(
					"C:\\Users\\windows\\git\\project1b\\LSIProject\\resources\\ami-launch-index.txt"));*/

			BufferedReader br=new BufferedReader(new FileReader(AMI_LAUNCH_INDEX_FILEPATH));

			ManageSession.currentServerAMIIndex = Integer.parseInt(br.readLine());
			Logger logger =  Logger.getLogger(ServerUtils.class.getName());
			logger.debug("Setting AMI Index from the local machine to "+ ManageSession.currentServerAMIIndex );
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletInitializationException("Failed to get the server's AMI-Launch Index");
		}

	}
	
	/**
	 * Method to read the server's Reboot Number from its file system.
	 * Sets the static currentServerRebootNumber variable to this value 
	 * @throws ServletInitializationException
	 */

	public static void setCurrentServerRebootNumber() throws ServletInitializationException{
		Logger logger =  Logger.getLogger(ServerUtils.class.getName());
		try {
			
			/* BufferedReader br=new BufferedReader(new FileReader(
					"C:\\Users\\windows\\git\\project1b\\LSIProject\\resources\\reboot.txt"));*/

			BufferedReader br=new BufferedReader(new FileReader(REBOOT_FILE_PATH));

			ManageSession.currentServerRebootNumber = Integer.parseInt(br.readLine());
			
			logger.debug("Setting AMI Index from the local machine to "+ ManageSession.currentServerRebootNumber );
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("exception hit while reboot number init "+ e );
			throw new ServletInitializationException("Failed to get the server's current Reboot Number");
		}

	}

	/**
	 * Method to build a map containing the server pool details from server's file system.	 
	 */
	
	public static void initServerPool() {
		JSONParser parser = new JSONParser();
		Logger logger =  Logger.getLogger(ServerUtils.class.getName());
		
		try {

			/* Object obj = parser.parse(new FileReader(
					"C:\\Users\\windows\\git\\project1b\\LSIProject\\resources\\simpleDbfile.json"));*/

			Object obj = parser.parse(new FileReader(SERVERS_JSON_FILE_PATH));
			
			
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray listOfMachines = (JSONArray) jsonObject.get("Items");

			for(Object machine:listOfMachines){
				JSONObject current = (JSONObject) machine;
				JSONArray keyValuePairs = (JSONArray) current.get("Attributes");
				JSONObject keyValuePair = (JSONObject) keyValuePairs.get(0);
				ServerPool.getInstance().putToServerPool(Long.parseLong((String) current.get("Name")), (String)keyValuePair.get("Value"));
			}
			logger.debug("Setting SERVER POOL DATA FROM JSON"+ ServerPool.getInstance().serverPoolMap );

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 *  Method that initializes the value of N,F,R,WQ and W
	 */
	public static void setFaultTolerantParameters(){
		SessionUtills.N = ServerPool.getInstance().serverPoolMap.size();
		if(SessionUtills.N == 0)
			throw new IllegalArgumentException("N is getting set to 0");

		//SessionUtills.F  = (long) Math.floor((SessionUtills.N-1)/2);
		SessionUtills.R  = SessionUtills.F+1;
		SessionUtills.WQ = SessionUtills.R;
		SessionUtills.W  = (2*SessionUtills.F+1);

		if(SessionUtills.W > SessionUtills.N)
			SessionUtills.W = SessionUtills.N;
	}
	
	/*
	 * Read in the resiliency factor expected from the system for the application
	 *  to make use of.
	 */

	public static void setResiliencyCount() throws ServletInitializationException {
		try {			
			BufferedReader br=new BufferedReader(new FileReader(RESILIENCY_FACTOR));
			SessionUtills.F = Integer.parseInt(br.readLine());
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletInitializationException("Failed to get the resiliency count");
		}		
	}
}
