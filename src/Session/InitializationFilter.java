package Session;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.apache.log4j.Logger;

import BackgroudTasks.ServletInitializer;

/**
 * Servlet Filter implementation class InitializationFilter
 */

// filter all requests thats pass through
@WebFilter("/*")
public class InitializationFilter implements Filter {
	
	Logger logger =  Logger.getLogger(this.getClass().getName());

    /**
     * Default constructor. 
     */
    public InitializationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		logger.debug("Within the filter.. if not printed something wrong with the redirection");
	
		String ErrorPage = "/initializationError.jsp";
		ServletContext context = request.getServletContext();
		boolean isSuccessfulInitialization = false;
		
		if(context.getAttribute(ServletInitializer.initializationAttr)!=null){
			isSuccessfulInitialization = (boolean) context.getAttribute(ServletInitializer.initializationAttr);
		}		

		if (! isSuccessfulInitialization) {
			logger.debug("Initialization was unsucessful therefore redirecting to error page..");
			request.getRequestDispatcher(ErrorPage).forward(request, response);
			return;
		}
		else {
			
			logger.debug("Successful initialization.. fwding the request from filter");
		    chain.doFilter(request, response);
		    return;
		}
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
