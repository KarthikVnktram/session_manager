package Session;

import java.util.concurrent.atomic.AtomicLong;

public class SessionCounter {
	private AtomicLong counter = new AtomicLong(0);
	private static SessionCounter sessionCounter = null;

	private SessionCounter(){
		
	}
	
	public long getCounter(){
		return counter.get();
	}
	
	public long incrementAndGet(){
		return counter.incrementAndGet();
	}
	
	public static SessionCounter getInstance(){
		
		if(sessionCounter == null){
			sessionCounter = new SessionCounter();
		} 
		return sessionCounter;
	}
	
}
