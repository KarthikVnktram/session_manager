package Session;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import SessionHandlers.CookieUtils;
import SessionHandlers.SessionObj;
import SessionHandlers.SessionTable;
import SessionHandlers.SessionUtills;
import exceptions.ReplicationSessionStateException;
import rpc.RPCManager;
import rpc.RPCReplyData;

/**
 * This class is the Servelet which handles all the requests from the client.
 * This contains the main business logic for session management
 */
@WebServlet("/ManageSession")
public class ManageSession extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static String COOKIE_ID = "CS5300PROJ1SESSION";
	public static SessionTable sessionTable = new SessionTable();
	public static int currentServerAMIIndex = -1;
	public static int currentServerRebootNumber = 0;
	public static String netID = "vkk22";
	public static String cookieCurrentDomain = "."+netID+".bigdata.systems";

	/**
	 * Default constructor. 
	 */
	public ManageSession() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		Logger logger =  Logger.getLogger(this.getClass().getName());
		Cookie currentCookie = null;
		String valueOfCookie = null; ///This would be the unique Identifier of the Cookie - cookie id
		PrintWriter reponseWriter = response.getWriter();
		String serverReadFrom = "";
		
		
			try{
				
				String responseString = null; // This is the string that would be sent back to the browser 
				String actionSelected = request.getParameter(SessionUtills.PARAM); // This is the action that the user has selected like Refresh, Replace, Logout 

				response.setContentType("text/html");

				Cookie[] cookies = request.getCookies();

				//Logic to iterate through the cookies find if the cookie that we have set exists
				if(cookies != null){
					for (int i = 0; i < cookies.length; i++) {
						String name = cookies[i].getName();

						if(name.equals(COOKIE_ID)){
							currentCookie = cookies[i];
							valueOfCookie = cookies[i].getValue();
							break;
						}

					}
				}

				SessionObj sessionObj=null;
				//Check and get the session Object from the session table
				RPCManager rpcManager = new RPCManager();		

				//session is not present in table ! but cookie still has session value OBJ !
				if(valueOfCookie != null){
					RPCReplyData reply = rpcManager.readSessionData(valueOfCookie);	
					//what if reply is null here !! redirect to error page !
					sessionObj=reply.getSessionObj();
					serverReadFrom="Session details read from server " + reply.getServerNumberReadFrom();

					if(sessionObj != null){
						//session object is present !
						if(SessionUtills.hasSessionExpired(sessionObj.getExpirationTime())){						
							logger.debug("Found Cookie but expired one. Will be creating a new session object");
							//expired need to recreate new sessionObject!
							valueOfCookie = null;            // CHECK THIS AGAIN ! CREATING DEPENDENCY ! 
							serverReadFrom = "";
						}
					}else{
						logger.debug("Couldn't find the session key in the servers.. May be right deletes");
						valueOfCookie = null;
						serverReadFrom = "";
					}
				}

				// Indicates that the session has not expired
				if(valueOfCookie != null){
					//This is the case to handle browser refresh

					logger.debug("Found a valid cookie to make use of...");
					if(actionSelected.equals(SessionUtills.REPLACE)){
						// This is the case to handle replace string
						logger.debug("Replace message action being invoked");
						//The message which the client might have sent
						String newMessage = request.getParameter(SessionUtills.MESSAGESTRING);
						synchronized (sessionObj) {
							sessionObj.incrementVersion();
							sessionObj.updateTimeOut();
							sessionObj.setMessage(newMessage);
						}

					}  else if(actionSelected.equals(SessionUtills.LOGOUT)){
						// This is the case to handle logout

						logger.debug("Logout action being performed..");
						synchronized (sessionObj) {
							currentCookie.setMaxAge(0); // This should clear up the cookie from the browser!
							sessionObj.setMessage("User Has Logged Out");
							response.addCookie(currentCookie);
							responseString = getEmptyResponseString(sessionObj) ;
							serverReadFrom = "";
						}
					} else {
						// On Refresh Button Click or on Browser refresh					
						logger.debug("Default block.. Refresh being performed");
						synchronized (sessionObj) {
							sessionObj.incrementVersion();
							sessionObj.updateTimeOut();
						}
					}
				} else {
					
					// This is a negative test case. when user clicks on logout twice or more back to back
					if(actionSelected.equals(SessionUtills.LOGOUT)){
						sessionObj  =  new SessionObj();
						sessionObj.setMessage("User Has Logged Out");
						responseString = getEmptyResponseString(sessionObj) ;
						responseString = responseString + SessionUtills.DELIMITER
											+ "" + SessionUtills.DELIMITER	
											+ "" + SessionUtills.DELIMITER											
											+ "" + SessionUtills.DELIMITER  
											+ "" + SessionUtills.DELIMITER
											+ "";
						reponseWriter.write(responseString);
						return;
					}
					
					//This is the initial load of the page, when the client is accessing the web page for the first time
					logger.debug("First time the application is being accessed.. Will create a new session and cookie.");
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.MINUTE, SessionUtills.REFRESH_EXP_TIME);
					// Adding Delta to Expiration Time 
					// Scenario: When we wait for WQ write responses, there will be some elapsed time for same.
					// Lets say my session timeout is 60 sec, and I'm waiting for WQ responses lets say 5s
					// In this case client receives session only for 55s, Thus adding DELTA
					calendar.add(Calendar.MILLISECOND, SessionUtills.REFRESH_DELTA);
					sessionObj = new SessionObj(SessionUtills.INITIAL_MESSAGE, calendar);
					serverReadFrom = "New Cookie created at Server " + currentServerAMIIndex;
				}

				if(!actionSelected.equals(SessionUtills.LOGOUT)){

					synchronized (sessionObj) {
						logger.debug("current action selected is "+actionSelected.toLowerCase());
						List<Long> replicatedInstances = rpcManager.replicateSessionData(sessionObj);
						logger.debug("Cookie ID="+COOKIE_ID+" Cookie String = "+CookieUtils.getCookieString(sessionObj, replicatedInstances));
						currentCookie = new Cookie(COOKIE_ID, CookieUtils.getCookieString(sessionObj, replicatedInstances));
//						currentCookie.setPath("/ManageSession");
						currentCookie.setDomain(cookieCurrentDomain);
						/*
						 * Mapping created
						 * server0.kv233.bigdata.systems => ec2-52-91-62-70.compute-1.amazonaws.com
						  server1.kv233.bigdata.systems => ec2-54-84-233-208.compute-1.amazonaws.com
						  server2.kv233.bigdata.systems => ec2-54-84-124-66.compute-1.amazonaws.com
						 * 
						 * 
						 * 
						 */
						
						currentCookie.setMaxAge(SessionUtills.REFRESH_EXP_TIME*60);
						
						logger.debug("current cookie set is "+currentCookie.getDomain());
						logger.debug("current Path set is "+currentCookie.getPath());
						response.addCookie(currentCookie);
						responseString = sessionObj.toString()+SessionUtills.DELIMITER+
										 CookieUtils.getCookieString(sessionObj, replicatedInstances)
										 + SessionUtills.DELIMITER + serverReadFrom 
										 + SessionUtills.DELIMITER + currentServerAMIIndex 
										 + SessionUtills.DELIMITER + SessionUtills.getCurrentRebootNumber() 
										 + SessionUtills.DELIMITER + currentCookie.getDomain();
						logger.debug("generated response "+responseString);
					}
				}else{
					
					//Replicate the Session Object with ExpirationTime = CurrentTime.
					synchronized (sessionObj) {
						List<Long> serverAMIIndexes = CookieUtils.getListOfReplicatedServersFromCookie(valueOfCookie);
						sessionObj.setExpirationTime(Calendar.getInstance());
						rpcManager.replicateSessionDataForLogout(serverAMIIndexes, sessionObj);
					}
					
					responseString = responseString + SessionUtills.DELIMITER
							+ "" + SessionUtills.DELIMITER											
							+ "" + SessionUtills.DELIMITER  
							+ "" + SessionUtills.DELIMITER
							+ "" + SessionUtills.DELIMITER
							+ "";
				}	
				logger.debug("Sent the response back to browser");
				reponseWriter.write(responseString);	

			}catch(ReplicationSessionStateException re){
				System.out.println("Within the exception handling...");	
				re.printStackTrace();
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MINUTE, SessionUtills.REFRESH_EXP_TIME);
				SessionObj newsessionObj = new SessionObj(SessionUtills.INITIAL_MESSAGE, calendar);
				if(currentCookie !=null){
					currentCookie.setMaxAge(0); // This should clear up the cookie from the browser!
					response.addCookie(currentCookie);
				}
				newsessionObj.setMessage(re.getMessage());
				
				String responseString = getEmptyResponseString(newsessionObj) ;
				responseString = responseString + SessionUtills.DELIMITER
									+ "" + SessionUtills.DELIMITER											
									+ "" + SessionUtills.DELIMITER  
									+ "" + SessionUtills.DELIMITER
									+ "" + SessionUtills.DELIMITER
									+ "";
							
				reponseWriter.write(responseString);
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Something really wrong..");
				logger.error(e.getMessage());			
			}

		
	}



	//Prepare and get the response string from the session object
	//This is for logout case
	String getEmptyResponseString(SessionObj sessionObj){

		return sessionObj.getMessage()+SessionUtills.DELIMITER+""+SessionUtills.DELIMITER+""+
				SessionUtills.DELIMITER+""+SessionUtills.DELIMITER+""+SessionUtills.DELIMITER+"";
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//never call this guy
	}

}
