This project involved creation of distributed, scalable and fault-tolerant website. All it did was to manage sessions internally and a sample webpage displayed the current session number, handled rebooting of different machines and gave an illusion that the website is being run on a single machine, even though it was distributed. 

RPCs were made between different machine to fetch the session data if the current requested machine did not contain the required Data.

This was an implementation of the SSM Protocol :

A paper titled : Session State: Beyond Soft State by Benjamin C. Ling, Emre Kıcıman and Armando Fox
{bling, emrek, fox}@cs.stanford.edu


Please check the README.pdf in the source folder for more information