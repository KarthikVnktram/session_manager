#!/bin/bash

# stub to be replaced by the user running the script

<<"COMMENT"
serverCount=replace the stuff with value for N
accessKey=replace the stuffwith value for accesskey
secretKey=replace the stuffwith value for secretkey
resiliencyFactor=replace the stuffwith value for resiliencyFactor
startupfilePath=replace the stuffwith value for startupFilePath
s3bucketName=replace the stuffwith value for s3bucketName
warfilePath=replace the stuffwith value for warfilePath
# if ssh to the instance is need then set the value for keypair
keypair=""
COMMENT


accessKey=xxxxxx
secretKey=xxxxxx
N=xxxxxx
F=xxxxxx
startupfilePath=xxxxxx
s3bucketName=xxxxxx
warfilePath=xxxxxx
# if ssh to the instance is need then set the value for keypair
keypair=xxxxx
securitygroup=xxxx

# check to ensure the path to startup script is a valid one
if [ -z $startupfilePath ] || [ ! -f $startupfilePath ]; then
	echo "Invalid path to the startup script.Specify path on local machine for the startup script."
	exit 1
fi 

# check to ensure the path to war file is a valid one
if [ -z $warfilePath ] || [ ! -f $warfilePath ]; then
	echo "Invalid path to the war file.Specify path on local machine for the war file."
	exit 1
fi

# check for the right values of N and F ie F<=N-1
maxServerCount=$((N-1));
if [ $F -gt $maxServerCount ]; then
	echo " Resililiency factor should be lesser than or equal to the server count minus 1 "
	exit 1
fi

# aws configuration being done to be able to use the CLI
aws configure set aws_access_key_id $accessKey 
aws configure set aws_secret_access_key $secretKey
aws configure set default.region us-east-1
 
# copy the application war to the s3 bucket that would be accessible by all cluster instances
aws s3 cp $warfilePath s3://$s3bucketName/LSIProject.war

# copy the startup script to s3 bucket to be used after reboot for application initialization
aws s3 cp $startupfilePath s3://$s3bucketName/install.sh


# making use of simple db to store in value of N that can be read by the instances during launch triggered by the user data execution
aws configure set preview.sdb true

# clearing up any previous states in the simple db
aws sdb delete-domain --domain-name LSIProject1
aws sdb delete-domain --domain-name ServerInstanceCount

# writing values for N,F and bucket to be used
aws sdb create-domain --domain-name ServerInstanceCount
aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="totalServers",Value=$N
sleep 3s
aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="Resiliency",Value=$F
sleep 3s
aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="s3BucketName",Value=$s3bucketName		

# for debugging
#aws sdb select --select-expression "SELECT * from ServerInstanceCount"	

# for debugging
#aws ec2 run-instances --region us-east-1 --image-id ami-08111162 --count $serverCount --instance-type t2.micro --user-data "file://$startupfilePath" --key-name amazon_instance --security-groups launch-wizard-1

# Creation of the cluster using run instances command

 if [ -z $keypair ]; then
	aws ec2 run-instances --region us-east-1 --image-id ami-08111162 --count $N --instance-type t2.micro --user-data "file://$startupfilePath"
 else
	aws ec2 run-instances --region us-east-1 --image-id ami-08111162 --count $N --instance-type t2.micro --user-data "file://$startupfilePath" --key-name $keypair --security-groups $securitygroup
fi  
 
	