#!/bin/bash

accessKey=AKIAJSGWHWIGYHYU7GNA
secretKey=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
aws configure set aws_access_key_id $accessKey
aws configure set aws_secret_access_key $secretKey
aws configure set default.region us-east-1
aws configure set preview.sdb true

#install Tomcat8
sudo yum -y install tomcat8-webapps tomcat8-docs-webapp tomcat8-admin-webapps
#install json parser
sudo yum -y install jq

#get the total number of instances
serverCount=`aws sdb select --select-expression "SELECT * from ServerInstanceCount" --consistent-read| jq '.Items[0].Attributes[0].Value'`
instancesCount=$(echo "$serverCount" | bc)
echo $instancesCount >> /var/tmp/errorlogs.txt

#Getting the IP address of machine
wget http://169.254.169.254/latest/meta-data/local-ipv4
machine_ip=$(head -n 1 local-ipv4)
echo $machine_ip

#Initialize Simple DB
aws configure set preview.sdb true
aws sdb create-domain --domain-name LSIProject1

#Getting the IP address of machine
wget http://169.254.169.254/latest/meta-data/ami-launch-index
ami_launch_index=$(head -n 1 ami-launch-index)
echo $ami_launch_index > /var/tmp/ami-launch-index.txt

resilenceFactor=0
#get the resilience factor
resilenceFactor=`aws sdb select --select-expression "SELECT * from ServerInstanceCount" --consistent-read| jq '.Items[0].Attributes[1].Value'`
resilenceFactor=$(echo "$resilenceFactor" | bc)
echo $resilenceFactor >> /var/tmp/resilienceCount.txt

#get the S3 Bucket Name from Simple Db
S3_BUCKET=`aws sdb select --select-expression "SELECT * from ServerInstanceCount" --consistent-read| jq '.Items[0].Attributes[2].Value'`
S3_BUCKET=$(echo "$S3_BUCKET" | bc)
echo $S3_BUCKET >> /var/tmp/s3_bucket.txt

#ensure that the entry gets added
VAR=true
while $VAR;
 do
 isInDb=`aws sdb select --select-expression "SELECT * from LSIProject1 where machine_ip='$machine_ip' " --consistent-read`
 if [ -n "$isInDb" ]; then
	echo "not empty"
	VAR=false
 else
	echo "empty"
	aws sdb put-attributes --domain-name LSIProject1 --item-name $ami_launch_index --attributes Name="machine_ip",Value="$machine_ip"
fi  
done

#wait until all the cluster nodes are up
sdbServerCount=`aws sdb select --select-expression "SELECT count(*) from LSIProject1" --consistent-read | jq '.Items[0].Attributes[0].Value'`
dbCount=$(echo "$sdbServerCount" | bc)

retryCounter=0
while [[ ( $dbCount -ne $instancesCount ) ]] && [[ ( $retryCounter -lt 20 ) ]]
	do 
		retryCounter=$((retryCounter+1));
		echo "waiting for 5s as currentDb count is $dbCount" >> /var/tmp/errorlogs.txt
		sleep 5s
		sdbServerCount=`aws sdb select --select-expression "SELECT count(*) from LSIProject1" --consistent-read | jq '.Items[0].Attributes[0].Value'`
		dbCount=$(echo "$sdbServerCount" | bc)	
		echo "After Waiting dbCount is $dbCount" >> /var/tmp/errorlogs.txt
done

echo "Found matching number of entries" >> /var/tmp/errorlogs.txt

#create the local json containing details of all the cluster servers

aws sdb select --select-expression "SELECT * from LSIProject1" --consistent-read > /var/tmp/serverEntries.json

if [ -f /var/tmp/reboot.txt ] 
then
	rebootCount=$(head -n 1 /var/tmp/reboot.txt)
	rebootCount=$((rebootCount+1))
	echo $rebootCount > /var/tmp/reboot.txt
else
	echo "0" > /var/tmp/reboot.txt
fi 

#Deploy war file from s3Bucket to TomcatWebapps Folder
sudo aws s3 cp s3://${S3_BUCKET}/LSIProject.war /usr/share/tomcat8/webapps/

#Download startup script into /tmp
sudo aws s3 cp s3://${S3_BUCKET}/startup.sh /var/tmp/
sudo chmod 777 /var/tmp/startup.sh 

# set permission to all users
sudo chmod -R a+rwx /var/tmp/

#start tomcat
#echo starting TOMCAT8 Server
sudo service tomcat8 start
echo "End of script"