#!/bin/bash
print_options () {
	echo -e \
	"Usage: launcher <options>
	where possible options include:
	  --help:\t\t     Print a synopsis of standard options
	  -N or --serverCount: \t\t Number of server nodes to be spawned in the custer using aws run instance
	  --accessKey:\t\t\t AWS access key
	  --secretKey:\t\t\t AWS access key value
	  --startupScript:\t\t\t  Absolute/relative path to the updated startup script to be used as user data
	  --s3BucketName:\t\t  s3 BucketName
	  --warFilePath:\t\t  Absolute/relative path to the war file
	  -F:\t\t Resiliency count (no of servers to write to) \n"
	exit 1
}

serverCount=0
resiliency=0
accessKey=""
secretKey=""
file=""
count="$#"
if [ $count \< 14 ]; then
	print_options
	exit 1
else
	while true; do			
		case "$1" in
			"")					
				break
				;;
			--help)
				print_options
				;;
			--serverCount)
				shift
				serverCount=$1
				shift				
				;;
			--accessKey)
				shift
				accessKey="$1"
				shift				
				;;
			--secretKey)
				shift
				secretKey="$1"
				shift				
				;;
			-N)
				shift
				serverCount=$1
				shift				
				;;
			-F)
				shift
				resiliency=$1
				shift				
				;;
			--startupScript)
				shift
				file=$1
				shift
				;;
			--s3BucketName)
				shift
				bucketName="$1"
				shift
				;;
			--warFilePath)
				shift
				warfilePath="$1"
				shift
				;;
			*)
				echo "Invalid usage. Run launcher --help for details"
				exit 1
				;;
		esac
	done
	if [ -z $file ] || [ ! -f $file ]; then
		echo "Invalid path to the startup script.Use --help for usage"
		exit 1
	fi 
	
	echo $file
	if [ $serverCount != 0 ]&& [ ! -z $accessKey ] && [ ! -z $secretKey ] && [ ! -z $bucketName ] && [ ! -z $warfilePath ]; then	
		
		aws configure set aws_access_key_id $accessKey 
		aws configure set aws_secret_access_key $secretKey
		aws configure set default.region us-east-1
 
		aws s3 cp $warfilePath s3://$bucketName/LSIProject.war
		aws s3 cp $file s3://$bucketName/startup.sh

		aws configure set preview.sdb true
		aws sdb delete-domain --domain-name LSIProject1
		aws sdb delete-domain --domain-name ServerInstanceCount

		aws sdb create-domain --domain-name ServerInstanceCount
		aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="totalServers",Value=$serverCount
		sleep 3s
		aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="Resiliency",Value=$resiliency
		aws sdb put-attributes --domain-name ServerInstanceCount --item-name "clusterDetails" --attributes Name="s3BucketName",Value=$bucketName		
				
		aws sdb select --select-expression "SELECT * from ServerInstanceCount"	
		aws ec2 run-instances --region us-east-1 --image-id ami-08111162 --count $serverCount --instance-type t2.micro --user-data "file://$file" --key-name amazon_instance --security-groups launch-wizard-1 
	else
		echo "Number of servers cannot be zero and aws access id and key is required. Use --help for options"
	fi		
fi